#include <stdio.h>
#include <math.h>

float f(float x, float y, float z);
double fx(double x, double y, double z);
double fy(double x, double y, double z);
double fz(double x, double y, double z);

int main(int argc, char const *argv[])
{
	for (float z = 1.5; z > -1.5; z-=0.1)
	{
		for (float x = -1.5; x < 1.5; x+=0.05)
		{
			if (f(x,0,z) < 0)
			{
				putchar('*');
			}
			else
				putchar(' ');
		}
		putchar('\n');
	}
	return 0;
}

float f(float x, float y, float z){
	float a = x*x + 9.0/4.0*y*y + z*z - 1;
	return a*a*a - x*x*z*z*z - 9.0/80.0*y*y*z*z*z;
}
